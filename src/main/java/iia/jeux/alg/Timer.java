package iia.jeux.alg;

public class Timer extends Thread {

	private int tempsJoueur;
	private final static int tempsMax = 590;

	public int getTempsJoueur() {
		return tempsJoueur;
	}

	public int getTempsMax() {
		return tempsMax;
	}

	public Timer(int i){
		this.tempsJoueur = i;
	}

	public void run() {
		while (tempsJoueur < tempsMax){
			try {
				sleep(1000);
				tempsJoueur++;
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}
	}
}
