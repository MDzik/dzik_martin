package  iia.jeux.alg;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.PlateauJeu;

public interface AlgoJeu {

    /** Renvoie le meilleur
     * @param p
     * @return
     * @throws InterruptedException
     */
	public CoupJeu meilleurCoup(PlateauJeu p);

}
