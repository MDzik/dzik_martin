package iia.jeux.alg;

import java.util.HashSet;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.CoupScore;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;
import jeux.fousfous.PlateauFousfousSave;

public class AlphaBetaFilesBlanc implements AlgoJeu {

	/**
	 * La profondeur de recherche par défaut
	 */
	private final static int PROFMAXDEFAUT = 4;

	// -------------------------------------------
	// Attributs
	// -------------------------------------------

	/**
	 * La profondeur de recherche utilisée pour l'algorithme
	 */
	private int profMax = PROFMAXDEFAUT;

	/**
	 * L'heuristique utilisée par l'algorithme
	 */
	private Heuristique h;

	public void setH(Heuristique h) {
		this.h = h;
	}

	/**
	 * Le joueur Min (l'adversaire)
	 */
	private Joueur joueurMin;

	/**
	 * Le joueur Max (celui dont l'algorithme de recherche adopte le point de
	 * vue)
	 */
	private Joueur joueurMax;

	/**
	 * Le nombre de noeuds développé par l'algorithme (intéressant pour se faire
	 * une idée du nombre de noeuds développés)
	 */
	private int nbnoeuds;

	/**
	 * Le nombre de feuilles évaluées par l'algorithme
	 */
	private int nbfeuilles;

	private int tempsPasee;

	// -------------------------------------------
	// Constructeurs
	// -------------------------------------------
	public AlphaBetaFilesBlanc(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
		this(h, joueurMax, joueurMin, PROFMAXDEFAUT);
	}

	public AlphaBetaFilesBlanc(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
		this.h = h;
		this.joueurMin = joueurMin;
		this.joueurMax = joueurMax;
		profMax = profMaxi;
		this.tempsPasee = 0;
		// System.out.println("Initialisation d'un MiniMax de profondeur " +
		// profMax);
	}

	// -------------------------------------------
	// Méthodes de l'interface AlgoJeu
	// -------------------------------------------
	@Override
	public CoupJeu meilleurCoup(PlateauJeu p) {
		Timer timer = new Timer(tempsPasee);
		timer.start();
		PlateauFousfousSave pf = (PlateauFousfousSave) p; /// TODO throw error
		int max = Integer.MIN_VALUE;
		HashSet<CoupJeu> maxs = null;
		if ((pf.getNbBlancs() + pf.getNbNoirs()) <= 8) {
			this.profMax = 14;
			if ((pf.getNbBlancs() + pf.getNbNoirs()) <= 8) {
				this.profMax = 100;
			}
		}
		for (CoupJeu coup : p.coupsPossibles(joueurMax)) {
			if (timer.getTempsJoueur() >= timer.getTempsMax()) {
				System.out.println("****************************** TIME ENDED ******************************");
				timer.interrupt();
				return coup;
			}
			PlateauJeu temp = p.copy();
			temp.joue(joueurMax, coup);

			int tmpScore = alphaBeta(temp, this.profMax - 1, Integer.MIN_VALUE, Integer.MAX_VALUE, false);
			System.out.print(tmpScore + " ");

			if (tmpScore > max) {
				maxs = new HashSet<CoupJeu>();
				max = tmpScore;
				maxs.add(coup);
				if (max > 1000000) {
					break;
				}
			}
			if (tmpScore == max) {
				maxs.add(coup);
			}
		}
		float tmp = 0;
		CoupJeu toJoue = (CoupJeu) maxs.toArray()[0];
		HashSet<CoupScore> fromFile = pf.getStatesFromFile(joueurMax.toString() + "\\" + pf.getSearchedFileName());
		if (fromFile != null) {
			for (CoupJeu cmax : maxs) {
				for (CoupScore cscr : fromFile) {
					if (cscr.equals(cmax)) {
						if (cscr.getScore() > tmp) {
							toJoue = cmax;
							tmp = cscr.getScore();
						}
					}
				}
			}
		}
		// System.out.println(maxs);
		// System.out.println(fromFile);
		tempsPasee = timer.getTempsJoueur();
		timer.interrupt();

		return toJoue;
	}

	// -------------------------------------------
	// Méthodes publiques
	// -------------------------------------------
	public String toString() {
		return "AlphaBeta(ProfMax=" + profMax + ")";
	}

	// -------------------------------------------
	// Méthodes internes
	// -------------------------------------------

	private int alphaBeta(PlateauJeu p, int profMax2, int alpha, int beta, boolean maximizingPlayer) {

		if (maximizingPlayer) {
			if (profMax2 <= 0 || p.coupsPossibles(joueurMax).isEmpty()) {
				return h.eval(p, joueurMax);
			}
			int v = Integer.MIN_VALUE;
			for (CoupJeu coup : p.coupsPossibles(joueurMax)) {
				PlateauJeu temp = p.copy();
				temp.joue(joueurMax, coup);
				v = Math.max(v, alphaBeta(temp, profMax2 - 1, alpha, beta, false));
				alpha = Math.max(alpha, v);
				if (beta <= alpha)
					break;

			}
			return v;
		} else {
			if (profMax2 <= 0 || p.coupsPossibles(joueurMin).isEmpty()) {
				return h.eval(p, joueurMin);
			}
			int v = Integer.MAX_VALUE;
			for (CoupJeu coup : p.coupsPossibles(joueurMin)) {
				PlateauJeu temp = p.copy();
				temp.joue(joueurMin, coup);
				v = Math.min(v, alphaBeta(temp, profMax2 - 1, alpha, beta, true));
				beta = Math.min(beta, v);
				if (beta <= alpha)
					break;

			}
			return v;

		}

	}


}
