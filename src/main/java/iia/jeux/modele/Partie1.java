package iia.jeux.modele;

import iia.jeux.modele.joueur.Joueur;

import java.io.IOException;
import java.util.ArrayList;

public interface Partie1 {

    /** initialise un plateau a partir d'un fichier texte
    *
    * @param fileName le nom du fichier a lire
    * @throws IOException
    */
	public void setFromFile(String fileName) throws IOException;

    /** sauve la configuration d'un plateau dans d'un fichier
    *
    * @param fileName le nom de fichier a sauvegarder
    * Le format doit etre compatibile avec la lecture
     * @throws IOException
    */
	public void saveToFile(String fileName) throws IOException;


    /** calcule les coups possibles pour le joueur <player> sur le plateau courant
    *
    * @param player le joueur qui joue
    */
    public String[] mouvementsPossibles (String player);


    /** joue un coup sur le plateau
    *
    * @param move le coup a jouer represente sous la forme "A1-B2"
    * @param player le joueur qui joue represente par "noir" ou "blanc"
    */
   public void play(String move, String player);


    /** indique si la partie est terminée
     *
     * @return vrai si la partie est terminée
     */
    public boolean finDePartie();


    /** indique si un coup est possible pour un joueur sur le plateau courant
    *
    * @param move le coup a jouer sous la forme "A1-B2"
    * @param player le joueur qui joue (represente par "noir" ou "blanc")
    */
	public boolean estValide(String move, String player);
}
