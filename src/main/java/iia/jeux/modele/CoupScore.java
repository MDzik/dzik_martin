package iia.jeux.modele;

public interface CoupScore {

	public CoupJeu getCoup();

	public float getScore();

	public void incrementTimesPlayed();

	public void incrementTimesWon();

	public boolean isImparable();

}
