package iia.jeux.modele;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public interface PlateauSaveOperations {

	/**
	 *
	 * @return
	 */
	public String getSearchedFileName();

	/**
	 *
	 * @return
	 */
	public String getPiecesPositions();

	/**
	 *
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public HashSet<CoupScore> getStatesFromFile(String filename) throws IOException;

	/**
	 * @throws IOException if file with results not found
	 * @throws Throwable if called on not ended game
	 *
	 */
	public void saveMovesToFile() throws IOException, Throwable;

}
