package jeux.fousfous;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.CoupScore;
import iia.jeux.modele.joueur.Joueur;

public class ScoresFousfousSaver extends PlateauFousfousSave {

	private boolean isWhiteWinner;

	public ScoresFousfousSaver(String fileName, int nbB, int nbN, boolean isWhite) throws IOException {
		super(fileName);
		this.nbBlancs = nbB;
		this.nbNoirs = nbN;
		this.isWhiteWinner = isWhite;
	}

	public void saveMoveToFile(Joueur j, String fileName) throws FileNotFoundException, UnsupportedEncodingException {
		CoupJeu toSave = this.playedMoves.getLast();
		HashSet<CoupScore> existing = null;
		try {
			existing = getStatesFromFile(fileName);
			/// Alors fichier existe
			if (existing != null) {

				for (CoupScore cs : existing) {
					if (cs.equals(toSave)) {
						cs.incrementTimesPlayed();
						if (this.isWhiteWinner && j.equals(this.joueurBlanc)) {
							cs.incrementTimesWon();
						} else {
							if (!this.isWhiteWinner && j.equals(this.joueurNoir)) {
								cs.incrementTimesWon();
							}
						}
						saveSetToFile(existing, j, fileName);
						return;
					}
				}
			} else {
				System.out.println("###############################################################################");
				System.out.println("###############################################################################");
				System.out.println("###############################################################################");
				return;
			}
		} catch (IOException e) {
//			existing = new HashSet<CoupScore>();
//			existing.add(new CoupScoreFousfous(toSave));
			e.printStackTrace();

//			saveSetToFile(existing, j, fileName);
		} catch (Throwable e) { /// Game not ended, nothing to return, we don't
								/// save anything
			System.out.println("Zlapano throwa - nie skonczono gry");
//			existing = new HashSet<CoupScore>();
//			existing.add(new CoupScoreFousfous(toSave));
//			e.printStackTrace();
//			saveSetToFile(existing, j, fileName);
		}
	}

	void saveSetToFile(HashSet<CoupScore> existing, Joueur j, String filename)
			throws UnsupportedEncodingException {
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			fw = new FileWriter("src\\main\\java\\jeux\\fousfous\\" + filename);
			bw = new BufferedWriter(fw);
			for (CoupScore cs : existing) {
				String s = cs.toString();
				bw.append(s);
			}
			// PrintWriter pw = null;

			// pw = new PrintWriter(filename, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Zapis sie nie powoid w save set to file");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ZlapanoIOExce w seveSetToFile");
			e.printStackTrace();
		} finally {
			try {

				if (bw != null)
					bw.flush();
				bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}

	}

	// private boolean isWhiteWinner() throws Throwable {
	// if (finDePartie()) {
	// if (nbBlancs == 0) {
	// return true;
	// }
	// return false;
	// }
	// throw new Exception("Game not Finished");
	// };

}
