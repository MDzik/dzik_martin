package jeux.fousfous;

import iia.jeux.alg.AlgoJeu;
import iia.jeux.alg.AlphaBetaBlanc;
import iia.jeux.alg.AlphaBetaNoir;
import iia.jeux.modele.*;
import iia.jeux.modele.joueur.Joueur;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PlateauFousfousSave extends PlateauFousfous implements PlateauSaveOperations {

	protected LinkedList<CoupJeu> playedMoves;

	public PlateauFousfousSave() {
		super();
		this.playedMoves = new LinkedList<CoupJeu>();
	}

	public PlateauFousfousSave(char[][] depuis, int nbB, int nbN) {
		super(depuis, nbB, nbN);
		this.playedMoves = new LinkedList<CoupJeu>();
	}

	public PlateauFousfousSave(String fileName) throws IOException {
		super(fileName);
		this.playedMoves = new LinkedList<CoupJeu>();
	}

	@Override
	public void joue(Joueur joueur, CoupJeu c) {
		playedMoves.add(c);
		super.joue(joueur, c);
	}

	@Override
	public PlateauJeu copy() {
		return new PlateauFousfousSave(this.damier, this.nbBlancs, this.nbNoirs);
	}

	/********************* PlateauSave methods ******************/

	@Override
	public String getSearchedFileName() {
		String toRet = "";
		for (char[] line : damier)
			toRet += new String(line);
		return toRet + ".txt";
	}

	@Override
	public String getPiecesPositions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashSet<CoupScore> getStatesFromFile(String filename) {

		HashSet<CoupScore> toRet = null;
		File f = new File("src\\main\\java\\jeux\\fousfous\\" + filename);
		if (f.exists() && !f.isDirectory()) {
			try {

				toRet = new HashSet<CoupScore>();

				BufferedReader br = null;
				FileReader fr = null;

				fr = new FileReader("src\\main\\java\\jeux\\fousfous\\" + filename);
				br = new BufferedReader(fr);

				String sCurrentLine;

				while ((sCurrentLine = br.readLine()) != null) {

					toRet.add(new CoupScoreFousfous(sCurrentLine));
				}
				br.close();
			} catch (IOException e) {
				// alors fichier n'existe pas, on va le creer
				// for (CoupJeu cj : coupsPossibles(joueur))
				return null;
			} catch (NullPointerException e) {
				// TODO: handle exception
				System.out.println("Zlapany wyjatek - null exception");
				e.printStackTrace();
				return null;

			}
		}

		// }

		return toRet;
	}

	/**
	 * can only be called after all moves are done.
	 *
	 * @throws Throwable
	 *             if game not ended
	 * @throws IOException
	 */
	@Override
	public void saveMovesToFile() throws Throwable, IOException {
		ScoresFousfousSaver tempP = new ScoresFousfousSaver("plateauInitial.txt", nbBlancs, nbNoirs,
				checkIfWhiteWinner());
		Joueur joueurs[] = new Joueur[2];
		joueurs[0] = tempP.joueurBlanc;
		joueurs[1] = tempP.joueurNoir;

		int currentJ = 0;

		for (CoupJeu cj : this.playedMoves) {
			/// I must take it now, as it represents current board state
			String fileName = joueurs[currentJ].toString() + "\\" + tempP.getSearchedFileName();
			// i must verify before playing if file exist
			File f = new File("src\\main\\java\\jeux\\fousfous\\" + fileName);
			// if not create it for current game state
			if (!f.exists() || f.isDirectory()) {
				System.out.println("Plik nie istnieje - dodano plik.");
				ArrayList<CoupJeu> temp = tempP.coupsPossibles(joueurs[currentJ]);
				HashSet<CoupScore> existing = new HashSet<CoupScore>();
				for (CoupJeu cj2 : temp){
					existing.add(new CoupScoreFousfous(cj2));
				}
				tempP.saveSetToFile(existing, joueurs[currentJ], fileName);
			}

			tempP.joue(joueurs[currentJ], cj);
			tempP.saveMoveToFile(joueurs[currentJ], fileName);
			currentJ = 1 - currentJ;
		}

	}

	private boolean checkIfWhiteWinner() throws Throwable {
		if (finDePartie()) {
			if (nbBlancs != 0) {
				return true;
			}
			return false;
		}
		throw new Exception("Game not Finished");
	};

}
