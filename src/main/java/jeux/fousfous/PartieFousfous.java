package jeux.fousfous;

import iia.jeux.alg.AlgoJeu;
import iia.jeux.alg.AlphaBetaBlanc;
import iia.jeux.alg.AlphaBetaFilesBlanc;
import iia.jeux.alg.AlphaBetaFilesNoir;
import iia.jeux.alg.AlphaBetaNoir;
import iia.jeux.alg.Minimax;
import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;
import jeux.fousfous.PlateauFousfous;

import java.io.IOException;
import java.util.ArrayList;

public class PartieFousfous {

	public static void main(String[] args) {
		int iter = 0;
		int winBlanc = 0;
		int winNoir = 0;

		while (iter < 850) {
			System.out.println("******************GRA NUMER: " + Integer.toString(iter)
					+ "********************************************");
			iter++;

			Joueur jBlanc = new Joueur("Blanc");
			Joueur jNoir = new Joueur("Noir");

			Joueur[] lesJoueurs = new Joueur[2];

			lesJoueurs[0] = jBlanc;
			lesJoueurs[1] = jNoir;

			AlgoJeu AlgoJoueur[] = new AlgoJeu[2];
			AlgoJoueur[0] = new AlphaBetaFilesBlanc(HeuristiquesFousfousFiles.hblanc, jBlanc, jNoir, 5);
			AlgoJoueur[1] = new AlphaBetaFilesNoir(HeuristiquesFousfousFiles.hnoir, jNoir, jBlanc, 5);
			// Il faut remplir la méthode !!!

			boolean jeufini = false;
			CoupJeu meilleurCoup = null;
			int jnum;

			PlateauJeu plateauCourant;
			try {
				plateauCourant = new PlateauFousfousSave("plateauInitial.txt");
				PlateauFousfous.setJoueurs(jBlanc, jNoir);
				jnum = 0; // On commence par le joueur Blanc(0) (arbitraire)
				while (!jeufini) {
					System.out.println("" + ((PlateauFousfous) plateauCourant).prettyPrint());
					System.out.println("C'est au joueur " + lesJoueurs[jnum] + " de jouer.");
					// Vérifie qu'il y a bien des coups possibles
					// Ce n'est pas tres efficace, mais c'est plus rapide... a
					// écrire...
					ArrayList<CoupJeu> lesCoupsPossibles = plateauCourant.coupsPossibles(lesJoueurs[jnum]);
					System.out.println("Coups possibles pour " + lesJoueurs[jnum] + " : " + lesCoupsPossibles);
					if (lesCoupsPossibles.size() > 0) {
						PlateauFousfousSave pf = (PlateauFousfousSave) plateauCourant;
						if (pf.getNbBlancs() + pf.getNbNoirs() <= 14){
							AlphaBetaFilesNoir abn = (AlphaBetaFilesNoir) AlgoJoueur[1];
							AlphaBetaFilesBlanc abb = (AlphaBetaFilesBlanc) AlgoJoueur[0];
							abn.setH(HeuristiquesFousfousFin.hnoir);
							abb.setH(HeuristiquesFousfousFin.hblanc);
							System.out.println("Zmieniono heurystyki");
						}
						// On écrit le plateau

						// Lancement de l'algo de recherche du meilleur coup
						System.out.println("Recherche du meilleur coup avec l'algo " + AlgoJoueur[jnum]);
						meilleurCoup = AlgoJoueur[jnum].meilleurCoup(plateauCourant);

						System.out.println("\nCoup joué : " + meilleurCoup + " par le joueur " + lesJoueurs[jnum]);
						plateauCourant.joue(lesJoueurs[jnum], meilleurCoup);
						// Le coup est effectivement joué
						jnum = 1 - jnum;

					} else {
						System.out.println("Le joueur " + lesJoueurs[jnum] + " ne peut plus jouer et abandone !");
						System.out.println("Le joueur " + lesJoueurs[1 - jnum] + " a gagné cette partie !");
						if (jnum == 0){
							winNoir++;
						}
						else{
							winBlanc++;
						}
						jeufini = true;
						PlateauFousfousSave pff = (PlateauFousfousSave) plateauCourant;
						try {
							pff.saveMovesToFile();
							System.out.print("Zapisano gre.");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							System.out.print("Zapis sie nie powiodl.");
							e.printStackTrace();
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							System.out.print("zapano throwa");
							e.printStackTrace();
						}

					}
				}

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}



		}

		System.out.println("# games won by white " + Integer.toString(winBlanc));
		System.out.println("# games won by black " + Integer.toString(winNoir));
	}
}
