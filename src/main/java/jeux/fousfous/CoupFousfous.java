package jeux.fousfous;
import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.CoupScore;

public class CoupFousfous implements CoupJeu{

	/****** Attributs *******/

	private int ligne1;

	private int colonne1;

	private int ligne2;

	private int colonne2;

	/****** Constructeur *******/

	public CoupFousfous(int l1, int c1, int l2,  int c2 ) {
		ligne1 = l1;
		colonne1 = c1;
		ligne2 = l2;
		colonne2 = c2;
	}

	public CoupFousfous(String s) {
		/// TODO cast exception maybe?
		colonne1 = (int) s.charAt(0) - 65; // ASCII A = 65
		ligne1 = (int) s.charAt(1) - 49; // ASCII 1 = 49
		colonne2 = (int) s.charAt(3) - 65;
		ligne2 = (int) s.charAt(4) - 49;
	}

	/****** Accesseurs *******/

	public int getLigne1() {
		return ligne1;
	}

	public int getColonne1() {
		return colonne1;
	}

	public int getLigne2() {
		return ligne2;
	}

	public int getColonne2() {
		return colonne2;
	}

	/****** Accesseurs *******/

	public String toString() {
		return "" + Character.toString((char)(colonne1+65))+Character.toString((char)(ligne1+49)) + "-" + Character.toString((char)(colonne2+65))+Character.toString((char)(ligne2+49));
	}


}

