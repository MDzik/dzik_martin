package jeux.fousfous;

import java.util.HashSet;

import iia.jeux.alg.Heuristique;
import iia.jeux.modele.CoupScore;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class HeuristiquesFousfousFiles {

	public static Heuristique hblanc = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					if (p2.getNbBlancs() == 0) {
						return Integer.MIN_VALUE + 1;
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
				int toAdd = 0;
				if (j != PlateauFousfousSave.joueurBlanc) {
					if (anyThreatWhite(p2)) {
						toAdd -= 4;
					}
					String fileName = j.toString() + "\\" + p2.getSearchedFileName();
					HashSet<CoupScore> css = p2.getStatesFromFile(fileName);
					if (css != null) {
						for (CoupScore cs : css) {
							if (cs.isImparable()) {
								toAdd -= 1100000;
							}
						}
					}
				} else {
					if (notProtectedNoir(p2)) {
						toAdd += 4;
					}
					String fileName = j.toString() + "\\" + p2.getSearchedFileName();
					HashSet<CoupScore> css = p2.getStatesFromFile(fileName);
					if (css != null) {
						for (CoupScore cs : css) {
							if (cs.isImparable()) {
								toAdd += 1100000;
							}
						}
					}

				}

				return 5 * differenceBlancs(p2) + menaceProtegeBlanc(p2) + toAdd;
			} else {
				/// TODO throw exception
				return -1000;
			}
		}

		private boolean anyThreatWhite(PlateauFousfous p2) {
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurBlanc, i, j)) {
							return true;
						}
					}
				}
			}
			return false;
		}

		/**
		 *
		 * @param p2
		 *            board to evaluate
		 * @return score for threatened white (negative points for threatened
		 *         white, positive for threatened+protected white and for
		 *         threatened but not protected blacks
		 */

		private int menaceProtegeBlanc(PlateauFousfous p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurBlanc, i, j)) {
							compter -= 2;
							if (p2.isThreatened(PlateauFousfous.joueurNoir, i, j)) {
								// dlaczego tu wywolujuemy na staticu?
								compter += 1;
							}
						}
					}
				}
			}
			return compter;
		}

		// zamiast naszego gdy to my bedzeimy grac
		private boolean notProtectedNoir(PlateauFousfous p2) {
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (!p2.isThreatened(PlateauFousfous.joueurBlanc, i, j)) {
							return true;
						}
					}
				}
			}
			return false;
		};

		private int differenceBlancs(PlateauFousfous p2) {
			return p2.getNbBlancs() - p2.getNbNoirs();
		}

		private int menaceBlanc(PlateauFousfous p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurBlanc, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

		private int protegerBlanc(PlateauFousfous p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurNoir, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}
	};

	public static Heuristique hnoir = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					// System.out.print(p2.prettyPrint());
					if (p2.getNbNoirs() == 0) {
						return Integer.MIN_VALUE + 1;
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
				int toAdd = 0;
				if (j == PlateauFousfousSave.joueurBlanc) {
					if (anyThreatNoir(p2)) {
						toAdd -= 4;
					}
					String fileName = j.toString() + "\\" + p2.getSearchedFileName();
					HashSet<CoupScore> css = p2.getStatesFromFile(fileName);
					if (css != null) {
						for (CoupScore cs : css) {
							if (cs.isImparable()) {
								toAdd -= 1100000;
							}
						}
					}

				} else {
					if (notProtectedWhite(p2)) {
						toAdd += 4;
					}
					String fileName = j.toString() + "\\" + p2.getSearchedFileName();
					HashSet<CoupScore> css = p2.getStatesFromFile(fileName);
					if (css != null) {
						for (CoupScore cs : css) {
							if (cs.isImparable()) {
								toAdd += 1100000;
							}
						}
					}

				}
				return differenceNoirs(p2) + protegerNoir(p2) - menaceNoir(p2) + toAdd;
				// return 0;
			}

			else {
				/// TODO throw ex
				return -1000;

			}

		}

		private boolean notProtectedWhite(PlateauFousfousSave p2) {
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (!p2.isThreatened(PlateauFousfous.joueurNoir, i, j)) {
							return true;
						}
					}
				}
			}
			return false;

		}

		private boolean anyThreatNoir(PlateauFousfousSave p2) {
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurNoir, i, j)) {
							return true;
						}
					}
				}
			}
			return false;
		}

		private int differenceNoirs(PlateauFousfous p2) {
			return p2.getNbNoirs() - p2.getNbBlancs();
		}

		private int menaceNoir(PlateauFousfous p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurNoir, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

		private int protegerNoir(PlateauFousfous p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (p2.isThreatened(PlateauFousfous.joueurBlanc, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

	};

}
