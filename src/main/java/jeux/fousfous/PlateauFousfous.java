package jeux.fousfous;

import iia.jeux.modele.*;
import iia.jeux.modele.joueur.Joueur;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PlateauFousfous implements PlateauJeu, Partie1 {

	/* *********** constantes *********** */

	/** Taille de la grille */
	public final static int TAILLE = 8;

	/* *********** Paramètres de classe *********** */

	private final static char VIDE = '-';
	private final static char BLANC = 'b';
	private final static char NOIR = 'n';

	/** Le joueur que joue "Blanc" */
	public static Joueur joueurBlanc;

	/** Le joueur que joue "noir" */
	public static Joueur joueurNoir;

	/* *********** Attributs *********** */

	/** le damier */
	protected char damier[][];

	protected int nbBlancs;
	protected int nbNoirs;


	/************* Constructeurs ****************/

	public PlateauFousfous() {
		damier = new char[TAILLE][TAILLE];
		for (int i = 0; i < TAILLE; i++)
			for (int j = 0; j < TAILLE; j++)
				damier[i][j] = VIDE;
		nbNoirs = 0;
		nbBlancs = 0;
	}

	public PlateauFousfous(char depuis[][], int nbB, int nbN) {
		damier = new char[TAILLE][TAILLE];
		for (int i = 0; i < TAILLE; i++)
			for (int j = 0; j < TAILLE; j++)
				damier[i][j] = depuis[i][j];
		nbNoirs = nbN;
		nbBlancs = nbB;
	}

	public PlateauFousfous(String fileName) throws IOException {
		damier = new char[TAILLE][TAILLE];
		nbNoirs = 0;
		nbBlancs = 0;
		setFromFile(fileName);
	}

	/************* Gestion des paramètres de classe** ****************/

	public static void setJoueurs(Joueur jb, Joueur jn) {
		joueurBlanc = jb;
		joueurNoir = jn;
	}

	public boolean isJoueurBlanc(Joueur jb) {
		return joueurBlanc.equals(jb);
	}

	public boolean isJoueurNoir(Joueur jn) {
		return joueurNoir.equals(jn);
	}

	/************* Méthodes de l'interface PlateauJeu ****************/

	public PlateauJeu copy() {
		return new PlateauFousfous(this.damier, this.nbBlancs, this.nbNoirs);
	}

	public boolean coupValide(Joueur joueur, CoupJeu c) {
		CoupFousfous cd = (CoupFousfous) c;
		int ligne1 = cd.getLigne1();
		int colonne1 = cd.getColonne1();
		int ligne2 = cd.getLigne2();
		int colonne2 = cd.getColonne2();
		return coupValide(joueur, ligne1, colonne1, ligne2, colonne2);
	}

	public ArrayList<CoupJeu> coupsPossibles(Joueur joueur) {
		ArrayList<CoupJeu> lesCoupsPossibles = new ArrayList<CoupJeu>();
		if (joueur.equals(joueurBlanc)) {
			int blancs = 0;
			for (int i = 0; i < TAILLE; i++) { // toutes les lignes
				for (int j = 0; j < TAILLE; j++) { // regarde sur une
														// colonne
					if ((damier[i][j] == BLANC)) {
						blancs++;
						lesCoupsPossibles.addAll(movementsPiece(joueur, i, j));
						if (blancs == nbBlancs) {
							return lesCoupsPossibles;
						}
					}
				}
			}
		} else { // Noir
			int noirs = 0;
			for (int i = 0; i < TAILLE; i++) { // toutes les lignes qui
													// passent
				for (int j = 0; j < TAILLE; j++) { // regarde sur toute colonne
					if (damier[i][j] == NOIR) {
						noirs++;
						lesCoupsPossibles.addAll(movementsPiece(joueur, i, j));
						if (noirs == nbNoirs) {
							return lesCoupsPossibles;
						}
					}
				}
			}
		}
		return lesCoupsPossibles;
	}

	private Collection<? extends CoupJeu> movementsPiece(Joueur joueur, int l, int c) {
		ArrayList<CoupJeu> lesCoupsPossibles = new ArrayList<CoupJeu>();

		char ami = ' ';
		char ennemie = ' ';
		if (joueur.equals(joueurBlanc)) {
			ami = BLANC;
			ennemie = NOIR;
		} else {
			ami = NOIR;
			ennemie = BLANC;
		}

		if (isThreatened(joueur, l, c)) {
			/// bas droite
			for (int i = l + 1, y = c + 1; i < TAILLE && y < TAILLE; i++, y++) {
				if (damier[i][y] == ennemie) {
					lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					break;
				}
				if (damier[i][y] == ami) {
					break;
				}
			}
			/// bas gauche
			for (int i = l + 1, y = c - 1; i < TAILLE && y >= 0; i++, y--) {
				if (damier[i][y] == ennemie) {
					lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					break;
				}
				if (damier[i][y] == ami) {
					break;
				}
			}
			/// haut droit
			for (int i = l - 1, y = c + 1; i >= 0 && y < TAILLE; i--, y++) {
				if (damier[i][y] == ennemie) {
					lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					break;
				}
				if (damier[i][y] == ami) {
					break;
				}
			}

			/// haut gauche
			for (int i = l - 1, y = c - 1; i >= 0 && y >= 0; i--, y--) {
				if (damier[i][y] == ennemie) {
					lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					break;
				}
				if (damier[i][y] == ami) {
					break;
				}
			}
		} else {
			/// bas droite
			for (int i = l + 1, y = c + 1; i < TAILLE && y < TAILLE; i++, y++) {
				if (damier[i][y] == VIDE) {
					if (isThreatened(joueur, i, y)) {
						lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					}
				} else {
					break;
				}
			}
			/// bas gauche
			for (int i = l + 1, y = c - 1; i < TAILLE && y >= 0; i++, y--) {
				if (damier[i][y] == VIDE) {
					if (isThreatened(joueur, i, y)) {
						lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					}
				} else {
					break;
				}
			}
			/// haut droit
			for (int i = l - 1, y = c + 1; i >= 0 && y < TAILLE; i--, y++) {
				if (damier[i][y] == VIDE) {
					if (isThreatened(joueur, i, y)) {
						lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					}
				} else {
					break;
				}
			}
			/// haut gauche
			for (int i = l - 1, y = c - 1; i >= 0 && y >= 0; i--, y--) {
				if (damier[i][y] == VIDE) {
					if (isThreatened(joueur, i, y)) {
						lesCoupsPossibles.add(new CoupFousfous(l, c, i, y));
					}
				} else {
					break;
				}
			}
		}
		return lesCoupsPossibles;
	}

	public boolean finDePartie() {
		return ((nbBlancs == 0) || (nbNoirs == 0));
	}

	public void joue(Joueur joueur, CoupJeu c) {
		CoupFousfous cd = (CoupFousfous) c;
		int ligne1 = cd.getLigne1();
		int colonne1 = cd.getColonne1();
		int ligne2 = cd.getLigne2();
		int colonne2 = cd.getColonne2();
		if (joueur.equals(joueurBlanc)) {
			if (damier[ligne2][colonne2] == NOIR) {
				nbNoirs--;
			}
			damier[ligne2][colonne2] = BLANC;
			damier[ligne1][colonne1] = VIDE;
		} else {
			if (damier[ligne2][colonne2] == BLANC) {
				nbBlancs--;
			}
			damier[ligne2][colonne2] = NOIR;
			damier[ligne1][colonne1] = VIDE;
		}
	}

	/************* Méthodes de l'interface Partie1 ****************/

	@Override
	public void setFromFile(String fileName) throws IOException {
		BufferedReader br = null;
		InputStream fr = null;

		fr = getClass().getResourceAsStream(fileName);
		br = new BufferedReader(new InputStreamReader(fr));

		String sCurrentLine;
		int counter = -1;

		while ((sCurrentLine = br.readLine()) != null) {
			if (!sCurrentLine.startsWith("%")) {
				counter++;
				for (int i = 0; i < TAILLE; i++) {
					damier[counter][i] = sCurrentLine.charAt(i + 2);
					if (sCurrentLine.charAt(i + 2) == 'b') {
						nbBlancs++;
					}
					if (sCurrentLine.charAt(i + 2) == 'n') {
						nbNoirs++;
					}
				}
			}
		}

		br.close();
	}

	@Override
	public void saveToFile(String fileName) throws IOException {

		PrintWriter pw = new PrintWriter(fileName, "UTF-8");
		pw.println("% Save");
		pw.println("% ABCDEFGH");
		for (int i = 0; i < TAILLE; i++) {
			String s = Integer.toString(i + 1) + " " + String.copyValueOf(damier[i]);
			pw.println(s);

		}
		pw.println("% ABCDEFGH");
		pw.close();
	}

	/**
	 * estValide transcrit le string en int puis appel la fonction coupValid qui
	 * sera utilise pour determiner si un coup est valide
	 */
	@Override
	public boolean estValide(String move, String player) {

		int c1 = (int) move.charAt(0) - 65; // ASCII A = 65
		int l1 = (int) move.charAt(1) - 49; // ASCII 1 = 49
		int c2 = (int) move.charAt(3) - 65;
		int l2 = (int) move.charAt(4) - 49;

		if (isInRange(c1) && isInRange(l1) && isInRange(c2) && isInRange(l2)) {
			if (isOnDiagonal(l1, c1, l2, c2)) {
				if (player == "blanc") {
					return coupValide(joueurBlanc, l1, c1, l2, c2);
				} else {
					return coupValide(joueurNoir, l1, c1, l2, c2);
				}
			}
		}
		return false;
	}

	@Override
	public String[] mouvementsPossibles(String player) {
		ArrayList<String> toReturn = new ArrayList<String>();
		if (player == "blanc") {
			for (CoupJeu coup : coupsPossibles(joueurBlanc)) {
				toReturn.add(coup.toString());
			}
		} else {
			for (CoupJeu coup : coupsPossibles(joueurNoir)) {
				toReturn.add(coup.toString());
			}
		}
		String[] stockArr = new String[toReturn.size()];
		stockArr = toReturn.toArray(stockArr);

		return stockArr;
	}

	@Override
	public void play(String move, String player) {
		int c1 = (int) move.charAt(0) - 65; // ASCII A = 65
		int l1 = (int) move.charAt(1) - 49; // ASCII 1 = 49
		int c2 = (int) move.charAt(3) - 65;
		int l2 = (int) move.charAt(4) - 49;
		System.out.println(l1);
		System.out.println(c1);
		System.out.println(l2);
		System.out.println(c2);
		this.joue(player == "blanc" ? joueurBlanc : joueurNoir, new CoupFousfous(l1, c1, l2, c2));
	}

	/* ********************* Autres méthodes ***************** */

	private boolean coupValide(Joueur joueur, int l1, int c1, int l2, int c2) {
		if (joueur.equals(joueurBlanc)) {
			if (damier[l1][c1] == 'b') {
				/// if target field is occupied by white, move is invalid
				if (damier[l2][c2] == BLANC) {
					return false;
				}
				/// verify move direction
				if (c1 < c2) {
					/// down right
					if (l1 < l2) {
						/// check if move diagonal is empty
						for (int i = l1 + 1, y = c1 + 1; i < l2 && y < c2; i++, y++) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == NOIR) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}
					/// up right
					else {
						/// check if move diagonal is empty
						for (int i = l1 - 1, y = c1 + 1; i > l2 && y < c2; i--, y++) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == NOIR) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}

					}
				} else {
					/// down left
					if (l1 < l2) {
						/// check if move diagonal is empty
						for (int i = l1 + 1, y = c1 - 1; i < l2 && y > c2; i++, y--) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == NOIR) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}
					/// up left
					else {
						/// check if move diagonal is empty
						for (int i = l1 - 1, y = c1 - 1; i > l2 && y > c2; i--, y--) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == NOIR) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}

				}
			} else
				return false;
		}
		/// joueur noir
		else {
			if (damier[l1][c1] == NOIR) {
				/// if target field is occupied by black, move is invalid
				if (damier[l2][c2] == NOIR) {
					return false;
				}
				/// verify move direction
				if (c1 < c2) {
					/// down right
					if (l1 < l2) {
						/// check if move diagonal is empty
						for (int i = l1 + 1, y = c1 + 1; i < l2 && y < c2; i++, y++) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == BLANC) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}
					/// up right
					else {
						/// check if move diagonal is empty
						for (int i = l1 - 1, y = c1 + 1; i > l2 && y < c2; i--, y++) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by black, we take it
						if (damier[l2][c2] == BLANC) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take black piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}

					}
				} else {
					/// down left
					if (l1 < l2) {
						/// check if move diagonal is empty
						for (int i = l1 + 1, y = c1 - 1; i < l2 && y > c2; i++, y--) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by white, we take it
						if (damier[l2][c2] == BLANC) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take white piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}
					/// up left
					else {
						/// check if move diagonal is empty
						for (int i = l1 - 1, y = c1 - 1; i > l2 && y > c2; i--, y--) {
							if (damier[i][y] != VIDE) {
								return false;
							}
						}
						/// if target field is occupied by white, we take it
						if (damier[l2][c2] == BLANC) {
							return true;
						}
						/// if target field is empty, we must check if moving
						/// piece can't take white piece
						if (isThreatened(joueur, l1, c1)) {
							return false;
						} else {
							return true;
						}
					}

				}
			} else
				return false;
		}

	}

	public boolean isThreatened(Joueur joueur, int l, int c) {
		if (joueur.equals(joueurBlanc)) {
			/// down right diagonal
			for (int i = l + 1, y = c + 1; i < TAILLE && y < TAILLE; i++, y++) {
				if (damier[i][y] == NOIR)
					return true;
				if (damier[i][y] == BLANC)
					break;
			}
			/// down left diagonal
			for (int i = l + 1, y = c - 1; i < TAILLE && y >= 0; i++, y--) {
				if (damier[i][y] == NOIR)
					return true;
				if (damier[i][y] == BLANC)
					break;
			}
			/// up right diagonal
			for (int i = l - 1, y = c + 1; i >= 0 && y < TAILLE; i--, y++) {
				if (damier[i][y] == NOIR)
					return true;
				if (damier[i][y] == BLANC)
					break;
			}
			/// up left diagonal
			for (int i = l - 1, y = c - 1; i >= 0 && y >= 0; i--, y--) {
				if (damier[i][y] == NOIR)
					return true;
				if (damier[i][y] == BLANC)
					break;
			}
		} else {
			/// down right diagonal
			for (int i = l + 1, y = c + 1; i < TAILLE && y < TAILLE; i++, y++) {
				if (damier[i][y] == BLANC)
					return true;
				if (damier[i][y] == NOIR)
					break;
			}
			/// down left diagonal
			for (int i = l + 1, y = c - 1; i < TAILLE && y >= 0; i++, y--) {
				if (damier[i][y] == BLANC)
					return true;
				if (damier[i][y] == NOIR)
					break;
			}
			/// up right diagonal
			for (int i = l - 1, y = c + 1; i >= 0 && y < TAILLE; i--, y++) {
				if (damier[i][y] == BLANC)
					return true;
				if (damier[i][y] == NOIR)
					break;
			}
			/// up left diagonal
			for (int i = l - 1, y = c - 1; i >= 0 && y >= 0; i--, y--) {
				if (damier[i][y] == BLANC)
					return true;
				if (damier[i][y] == NOIR)
					break;
			}
		}
		return false;
	}

	public String toString() {
		String retstr = new String("");
		for (int i = 0; i < TAILLE; i++) {
			for (int j = 0; j < TAILLE; j++)
				if (damier[i][j] == VIDE)
					retstr += "-";
				else if (damier[i][j] == BLANC)
					retstr += "B";
				else // damier[i][j] == NOIR
					retstr += "N";
			retstr += "\n";
		}
		return retstr;
	}

	public String prettyPrint(){
		String retStr = new String("\n |A|B|C|D|E|F|G|H|\n");
		for (int i = 0; i < TAILLE; i++) {
			retStr += "" + (i+1);
			for (int j = 0; j < TAILLE; j++)
				if (damier[i][j] == VIDE)
					retStr += "|-";
				else if (damier[i][j] == BLANC)
					retStr += "|B";
				else // damier[i][j] == NOIR
					retStr += "|N";
			retStr += "|" + (i+1) + "\n";
		}
		return  retStr + " |A|B|C|D|E|F|G|H| \n	blancs: " + nbBlancs + "  noirs: " + nbNoirs;

	}

	public void printPlateau(PrintStream out) {
		out.println(this.toString());
	}

	public int nbCoupsBlanc() {
		int nbCoups = 0;
		for (int i = 0; i < TAILLE; i++) {
			for (int j = 0; j < TAILLE - 1; j++) {
				if (damier[i][j] == VIDE && damier[i][j + 1] == VIDE)
					nbCoups++;
			}
		}
		return nbCoups;
	}

	public int nbCoupsNoir() {
		int nbCoups = 0;
		for (int i = 0; i < TAILLE; i++) {
			for (int j = 0; j < TAILLE - 1; j++) {
				if (damier[j][i] == VIDE && damier[j + 1][i] == VIDE)
					nbCoups++;
			}
		}
		return nbCoups;
	}

	/**
	 *
	 * @param l1
	 * @param c1
	 * @param l2
	 * @param c2
	 * @return vrai si les deux cases sont sur une meme diagonale
	 */
	private boolean isOnDiagonal(int l1, int c1, int l2, int c2) {
		if (c1 - l1 == c2 - l2)
			return true;
		if ((9 - c1) - l1 == (9 - c2) - l2)
			return true;
		return false;
	}

	/**
	 *
	 * @param i
	 * @return vrai si i est entre 0 et <TAILLE>
	 */
	private boolean isInRange(int i) {
		return (i < TAILLE) && (0 <= i);
	}


	/************* Accesseurs ****************/

	public int getNbBlancs() {
		return nbBlancs;
	}

	public int getNbNoirs() {
		return nbNoirs;
	}

	boolean estPionBlanc(int i, int j){
		if (damier[i][j] == BLANC){
			return true;
		}else{
			return false;
		}
	}

	boolean estPionNoir(int i, int j){
		if (damier[i][j] == NOIR){
			return true;
		}else{
			return false;
		}
	}

}
