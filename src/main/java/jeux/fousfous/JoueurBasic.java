package jeux.fousfous;

import java.io.IOException;

import iia.jeux.alg.AlgoJeu;
import iia.jeux.alg.AlphaBetaBlanc;
import iia.jeux.alg.AlphaBetaFilesNoir;
import iia.jeux.alg.AlphaBetaNoir;
import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.joueur.Joueur;

public class JoueurBasic implements IJoueur {

	private int myColour;
	private PlateauFousfousSave plateau;
	private AlgoJeu algo;
	private Joueur joueur;
	private Joueur opponent;



	@Override
	public void initJoueur(int mycolour) {
		System.out.println("tworze sie: " );

		this.myColour = mycolour;
		try {
			System.out.println("tworze plansze: " );
			this.plateau = new PlateauFousfousSave("plateauInitial.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (mycolour == -1){
			System.out.println("jestem bialy: " );
			this.joueur = new Joueur("blanc");
			this.opponent = new Joueur("noir");
			PlateauFousfousSave.setJoueurs(joueur, opponent);
			this.algo = new AlphaBetaBlanc(HeuristiqueFousfousFiles.hblanc, joueur, opponent) ;
		}
		else {
			this.joueur = new Joueur("noir");
			this.opponent = new Joueur("blanc");
			PlateauFousfousSave.setJoueurs(opponent, joueur);
			this.algo = new AlphaBetaFilesNoir(HeuristiqueFousfousFiles.hnoir, joueur, opponent);
		}

	}

	@Override
	public int getNumJoueur() {
		return myColour;
	}

	@Override
	public String choixMouvement() {
		System.out.println("Je cherche: " );
		CoupJeu meilleurCoup = algo.meilleurCoup(plateau);
		System.out.println("Je joue: " );
		plateau.joue(joueur, meilleurCoup);
		//System.out.println("I decided to play: " + meilleurCoup.toString());
		// System.out.println(plateau.prettyPrint());
		return meilleurCoup.toString();
	}

	@Override
	public void declareLeVainqueur(int colour) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouvementEnnemi(String coup) {
		this.plateau.play(coup, opponent.toString());
		System.out.println("Oppenent  played: " + coup);
		// System.out.println(plateau.prettyPrint());

	}

	@Override
	public String binoName() {
		return "Dzik-Martin";
	}



}
