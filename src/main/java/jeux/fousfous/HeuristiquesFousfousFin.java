package jeux.fousfous;

import iia.jeux.alg.Heuristique;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class HeuristiquesFousfousFin {

	public static Heuristique hblanc = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					if (p2.getNbBlancs() == 0) {
						return Integer.MIN_VALUE + 100 + differenceBlancs(p2);
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
				return 0;
			} else {
				/// TODO throw exception
				return -1000;
			}
		}

		private int differenceBlancs(PlateauFousfous p2) {
			return p2.getNbBlancs() - p2.getNbNoirs();
		}

	};



	public static Heuristique hnoir = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					if (p2.getNbNoirs() == 0) {
						return Integer.MIN_VALUE + 100 + differenceNoirs(p2);
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
				return 0;
			} else {
				/// TODO throw exception
				return -1000;
			}
		}

		private int differenceNoirs(PlateauFousfous p2) {
			return p2.getNbNoirs() - p2.getNbBlancs();
		}

	};
}
