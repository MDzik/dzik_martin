package jeux.fousfous;

import iia.jeux.modele.CoupJeu;
import iia.jeux.modele.CoupScore;

public class CoupScoreFousfous implements CoupScore {

	private CoupJeu coup;
	private int timesPlayed;
	private int timesWon;

	public CoupScoreFousfous(String s) {
		String[] data = s.split("\\|");
		this.coup = new CoupFousfous(data[0]);
		this.timesWon = Integer.parseInt(data[1]);
		this.timesPlayed = Integer.parseInt(data[2]);

	}

	public CoupScoreFousfous(CoupJeu c) {
		this.coup = c;
		this.timesPlayed = 1;
		this.timesWon = 1;
	}

	@Override
	public CoupJeu getCoup() {
		return coup;
	}

	@Override
	public float getScore() {
		return (float)timesWon/timesPlayed;
	}

	@Override
	public void incrementTimesPlayed() {
		timesPlayed++;
	}

	@Override
	public void incrementTimesWon() {
		timesWon++;
	}

	@Override
	public String toString(){
		return  coup.toString() + '|' + Integer.toString(timesWon) + '|' + Integer.toString(timesPlayed) + "\n";

	}

	@Override
    public boolean equals(Object cj) {
        if (!(cj instanceof CoupJeu)) {
            return false;
        }
        CoupFousfous et = (CoupFousfous) cj;
        String s1 = this.coup.toString();
        String s2 = et.toString();
        if (!this.coup.toString().equals( et.toString())) {
            return false;
        }
        return true;
    }

	@Override
	public boolean isImparable() {
		if ((Math.abs(1 - getScore()) < 0.15) && timesPlayed > 1){
			return true;
		}
		return false;
	}

}
