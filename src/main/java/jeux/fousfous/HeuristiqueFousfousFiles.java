package jeux.fousfous;

import iia.jeux.alg.Heuristique;
import iia.jeux.modele.PlateauJeu;
import iia.jeux.modele.joueur.Joueur;

public class HeuristiqueFousfousFiles {

	public static Heuristique hblanc = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					if (p2.getNbBlancs() == 0) {
						return Integer.MIN_VALUE + 1;
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
//				if (p2.getNbBlancs() + p2.getNbNoirs() <= 15) {
//					if (j == p2.joueurBlanc) {
//						if (anyThreatWhite(p2)) {
//							return 5 + differenceBlancs(p2) + menaceProtegeBlanc(p2);
//						} else {
//							return differenceBlancs(p2) + menaceProtegeBlanc(p2);
//						}
//					} else {
//						if (anyThreatWhite(p2)) {
//							return -5 + differenceBlancs(p2) + menaceProtegeBlanc(p2);
//						} else {
//							return differenceBlancs(p2) + menaceProtegeBlanc(p2);
//						}
//					}
//				}
//				else {
//					return 3*differenceBlancs(p2) +menaceProtegeBlanc(p2);
//				}
				return 0;
			} else {
				/// TODO throw exception
				return -1000;
			}
		}

		private boolean anyThreatWhite(PlateauFousfousSave p2) {
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurBlanc, i, j)) { ///to jest dziwne
							return true;
						}
					}
				}
			}
			return false;
		}

		private int menaceProtegeBlanc(PlateauFousfousSave p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurBlanc, i, j)) {
							compter -= 4;
							if (p2.isThreatened(PlateauFousfousSave.joueurNoir, i, j)) {
								compter += 4;
							}
						} else {
							if (p2.isThreatened(PlateauFousfousSave.joueurNoir, i, j)) {
								compter += 1;
							}
						}
					}
				}
			}
			return compter;
		}

		private int differenceBlancs(PlateauFousfousSave p2) {
			return p2.getNbBlancs() - p2.getNbNoirs();
		}

		private int menaceBlanc(PlateauFousfousSave p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurBlanc, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

		private int protegerBlanc(PlateauFousfousSave p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionBlanc(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurNoir, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}
	};

	public static Heuristique hnoir = new Heuristique() {

		public int eval(PlateauJeu p, Joueur j) {
			if (p.getClass().equals(PlateauFousfousSave.class)) {
				PlateauFousfousSave p2 = (PlateauFousfousSave) p;
				if (p2.finDePartie()) {
					// System.out.print(p2.prettyPrint());
					if (p2.getNbNoirs() == 0) {
						return Integer.MIN_VALUE + 1;
					} else {
						return Integer.MAX_VALUE - 1;
					}
				}
				///return differenceNoirs(p2) + protegerNoir(p2) - menaceNoir(p2);
				return 0;
			}

			else {
				/// TODO throw ex
				return -1000;

			}

		}

		private int differenceNoirs(PlateauFousfousSave p2) {
			return p2.getNbNoirs() - p2.getNbBlancs();
		}

		private int menaceNoir(PlateauFousfousSave p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurNoir, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

		private int protegerNoir(PlateauFousfousSave p2) {
			int compter = 0;
			for (int i = 0; i < p2.TAILLE; i++) {
				for (int j = 0; j < p2.TAILLE; j++) {
					if (p2.estPionNoir(i, j)) {
						if (p2.isThreatened(PlateauFousfousSave.joueurBlanc, i, j)) {
							compter++;
						}
					}
				}
			}
			return compter;
		}

	};

}
