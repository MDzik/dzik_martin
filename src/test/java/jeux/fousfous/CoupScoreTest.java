package jeux.fousfous;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CoupScoreTest {

	private CoupScoreFousfous coup;

	@Before
	public void init() {
		coup = new CoupScoreFousfous("B2-C3|4|8");
	}

	@Test
	public void testGetScore() {
		assertEquals(0.5, coup.getScore(), 0);
	}

	@Test
	public void testTimesWonIncrement() {
		coup.incrementTimesWon();
		assertEquals(5.0/8.0, coup.getScore(), 0);
	}

}
